import React from 'react';
import {View, Text, ImageBackground, Image} from 'react-native';
import styles from './styles';
const Post = ({image, type, bed, bedroom,title, oldPrice, newPrice, totalPrice}) => {
  return (
    <View style={styles.container}>
      {/* Image */}
      <Image
        style={styles.image}
        source={{
          uri:
            image,
        }}
      />
      {/* Bed and bedroom */}
      <Text style={styles.bedrooms}>{bed} bed {bedroom} bedroom</Text>
      {/* Type and Description */}
      <Text style={styles.description} numberOfLines={2}>
        {type}. {title}
      </Text>
      
      <Text style={styles.prices}>
        <Text style={styles.oldPrice}>${oldPrice}</Text>{' '}
        <Text style={styles.newPrice}>${newPrice}</Text> / night
      </Text>
      {/* Total price */}
      <Text style={styles.totalPrice}>${totalPrice} total</Text>
    </View>
  );
};
export default Post;
