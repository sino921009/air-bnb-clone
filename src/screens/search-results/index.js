import React from 'react';
import {View, Text, FlatList} from 'react-native';
import styels from './styles';
import Post from '../../../src/components/post';
import feed from '../../../assets/data/feed';
export default SearchResultsScreen = (props) => {
  return (
    <View>
      <FlatList
        data={feed}
        renderItem={({item}) => {
          return <Post key={item.id} {...item} />;
        }}
      />
      {/* {

          feed.map((item, index) => {
            return <Post key={item.id} {...item}/>
          })
        } */}
    </View>
  );
};
