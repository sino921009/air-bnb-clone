import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
    container: {
        margin: 20
    },
    textInput: {
        fontSize: 20
    }, 
    list: {
        marginTop: 20
    },
    listItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'flex-start',
        borderBottomColor:'lightgrey',
        borderBottomWidth: 1,
        paddingVertical: 15
    }, 
    iconContainer: {
        backgroundColor: '#d4d4d4',
        borderRadius: 10,
        marginRight: 20,
        padding: 7
    },
    listItemDescription:{

    }
})