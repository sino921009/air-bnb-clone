import React, {useState} from 'react';
import {View, Text, TextInput, FlatList} from 'react-native';
import Entype from 'react-native-vector-icons/Entypo';
import styles from './styles';
import search from '../../../assets/data/search';
export default DestinationSearchScreen = (props) => {
  const [keyword, setKeyword] = useState('');
  const setKeywordHandler = (enteredText) => {
    setKeyword(enteredText);
  };
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textInput}
        placeholder="Where are you going?"
        onChangeText={setKeywordHandler}
        value={keyword}
        autoCorrect={false}ß
      />
      <FlatList
        style={styles.list}
        data={search}
        renderItem={({item}) => 
        <View style={styles.listItem}>
            <View style={styles.iconContainer}>
                <Entype name="location-pin" size={30}/>
            </View>
            <Text style={styles.listItemDescription}>{item.description}</Text>
        </View>
        
        }
      />
    </View>
  );
};
