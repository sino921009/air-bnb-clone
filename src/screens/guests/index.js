import React, {useState} from 'react';
import {View, Text, Pressable} from 'react-native';
import styles from './styles';
export default GuestsScreen = (props) => {
  const [adults, setAdults] = useState(0);
  const [children, setChildren] = useState(0);
  const [infants, setInfants] = useState(0);
  const setAdultsClickHandler = (increment, operation) => {
    // if (operation == 'plus') {
    //   if (adults + increment > 40) {
    //     return;
    //   }
    // } else if (operation == 'minus') {
    //   if (adults + increment < 0) {
    //     return;
    //   }
    // }
    setAdults((currentAdults) => {
      if (operation == 'plus') {
        currentAdults = Math.min(40, adults + increment);
      } else if (operation == 'minus') {
        currentAdults = Math.max(0, adults + increment);
      }
      return currentAdults;
    });
  };

  return (
    <View style={{}}>
      <View style={styles.row}>
        <View>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            Adults
          </Text>
          <Text
            style={{
              color: 'lightgrey',
            }}>
            Ages 13 or above
          </Text>
        </View>
        {/* Buttons */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {/* - */}
          <Pressable
            onPress={setAdultsClickHandler.bind(this, -1, 'minus')}
            style={styles.button}>
            <Text
              style={{
                fontSize: 20,
                color: 'lightgrey',
              }}>
              -
            </Text>
          </Pressable>
          {/* value */}
          <Text
            style={{
              fontSize: 16,
              marginHorizontal: 20,
            }}>
            {adults}
          </Text>
          {/* + */}
          <Pressable
            onPress={setAdultsClickHandler.bind(this, 1, 'plus')}
            style={styles.button}>
            <Text
              style={{
                fontSize: 20,
                color: 'lightgrey',
              }}>
              +
            </Text>
          </Pressable>
        </View>
      </View>
      <View style={styles.row}>
        <View>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            Children
          </Text>
          <Text
            style={{
              color: 'lightgrey',
            }}>
            Ages 2 to 12
          </Text>
        </View>
        {/* Buttons */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {/* - */}
          <Pressable
            onPress={() => setChildren(Math.max(0, children - 1))}
            style={styles.button}>
            <Text
              style={{
                fontSize: 20,
                color: 'lightgrey',
              }}>
              -
            </Text>
          </Pressable>
          {/* value */}
          <Text
            style={{
              fontSize: 16,
              marginHorizontal: 20,
            }}>
            {children}
          </Text>
          {/* + */}
          <Pressable
           onPress={() => setChildren(Math.min(10, children + 1))}
            style={styles.button}>
            <Text
              style={{
                fontSize: 20,
                color: 'lightgrey',
              }}>
              +
            </Text>
          </Pressable>
        </View>
      </View>
      <View style={styles.row}>
        <View>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            Infants
          </Text>
          <Text
            style={{
              color: 'lightgrey',
            }}>
            Under 2
          </Text>
        </View>
        {/* Buttons */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {/* - */}
          <Pressable
           onPress={() => setInfants(Math.max(0, infants - 1))}
            style={styles.button}>
            <Text
              style={{
                fontSize: 20,
                color: 'lightgrey',
              }}>
              -
            </Text>
          </Pressable>
          {/* value */}
          <Text
            style={{
              fontSize: 16,
              marginHorizontal: 20,
            }}>
            {infants}
          </Text>
          {/* + */}
          <Pressable
            onPress={() => setInfants(Math.min(10, infants + 1))}
            style={styles.button}>
            <Text
              style={{
                fontSize: 20,
                color: 'lightgrey',
              }}>
              +
            </Text>
          </Pressable>
        </View>
      </View>
    </View>
  );
};
