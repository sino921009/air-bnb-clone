/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  FlatList
} from 'react-native';

import {
  Colors,

} from 'react-native/Libraries/NewAppScreen';
import Entype from 'react-native-vector-icons/Entypo';
import HomeScreen from './src/screens/home/index';
import Post from './src/components/post';
import SearchResultsScreen from './src/screens/search-results';
import DestinationSearchScreen from './src/screens/destination-search';
import GuestsScreen from './src/screens/guests'
const App: () => React$Node = () => {
  return (
    <>
      <SafeAreaView>
        <GuestsScreen/>
      </SafeAreaView>
    </>
  );
};

export default App;
